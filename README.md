# Generic Extractor configurations for Keboola APIs

## Configurations

### `all-components-root-call.json` 

Storage API root call that retrieves all available components in the Keboola Connection, even the non-published ones. 
**NO PARAMETERS NEEDED**


### `all-public-components-dev-portal.json` 

Dev Portal API call retrieving all public components. **NO PARAMETERS NEEDED**

### `users-eu-region.json` 

Get all users in EU region.

**Parameters**
- `[YOUR_ORG_ID]` - replace with your organization ID
- `Manage token` - your manage token